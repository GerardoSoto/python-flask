#!/usr/bin/python
#-*- coding: utf-8 -*-

from flask import Flask, request, make_response, redirect, render_template


app = Flask(__name__) #__name__ = main.py


@app.route('/')
def index():
    """Función principal del programa"""
    user_ip = request.remote_addr
    response = make_response(redirect('/hello'))
    #creación de una cookie para la IP del usuario:
    response.set_cookie('user_ip',user_ip)
    #restuesta type(): Flask:
    return response


@app.route('/hello')
def hello():
    """Mensaje de bienvenida y IP del usuario"""
    #Obtencion de la IP del usuario de las cookies:
    user_ip = request.cookies.get('user_ip')
    context = {
        'user_ip': user_ip,
        'to_dos': to_do,
    }
    #respuesta type(): String:
    return render_template('hello.html', **context)

to_do = ['>git pull','>git add .', '>git commit -m', '>git push']

#if __name__ == '__main__':
#    app.run(port = 7000, debug = True)# -> $ flask run -h=0.0.0.0 -p=5000
    #app.run()

